
import { sleep, group } from "k6";
import { check } from 'k6';
import http from "k6/http";
import { parseHTML } from 'k6/html';

export const options = {
  
};
const VAULT_TOKEN = __ENV.vault_token;
let username;
let password;
let response;

export default function main() {

  const vars = {};
  response = http.get(
    "https://prod-vault.accruentsystems.com/v1/nonprod/data/MaintenanceConnection",
      {
        headers: {
          "accept-encoding": "gzip, deflate, br",
          "accept-language": "en",
          connection: "keep-alive",
          "X-Vault-Namespace": "/qe",
          "X-Vault-Token": VAULT_TOKEN,
        },
      }
    );

    
   
    let username = response.json().data.data.PERF_Username
    let password = response.json().data.data.PERF_Password
    
  group(
    "page_1 - https://login-qa.maintenanceconnection.com/Login",
    function () {
      response = http.get("https://login-qa.maintenanceconnection.com/Login", {
        headers: {
          "upgrade-insecure-requests": "1",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
        },
      });

      vars["LoginClass"] = response
        .html()
        .find("input[name=LoginClass]")
        .first()
        .attr("value");

      vars["Dlargs"] = response
        .html()
        .find("input[name=Dlargs]")
        .first()
        .attr("value");

      vars["__RequestVerificationToken"] = response
        .html()
        .find("input[name=__RequestVerificationToken]")
        .first()
        .attr("value");

      sleep(11.3);
    }
  );

  group("page_2 - https://login-qa.maintenanceconnection.com/", function () {
    response = http.post(
      "https://login-qa.maintenanceconnection.com/",
      {
        LoginClass: `${vars["LoginClass"]}`,
        Dlargs: `${vars["Dlargs"]}`,
        MemberId: `${username}`,
        __RequestVerificationToken: `${vars["__RequestVerificationToken"]}`,
      },
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          origin: "https://login-qa.maintenanceconnection.com",
          "upgrade-insecure-requests": "1",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
        },
      }
    );

    vars["LoginClass2"] = response
      .html()
      .find("input[name=LoginClass]")
      .first()
      .attr("value");

    vars["Dlargs2"] = response
      .html()
      .find("input[name=Dlargs]")
      .first()
      .attr("value");

    vars["__RequestVerificationToken2"] = response
      .html()
      .find("input[name=__RequestVerificationToken]")
      .first()
      .attr("value");

    sleep(11.8);

    response = http.post(
      "https://login-qa.maintenanceconnection.com/",
      {
        LoginClass: `${vars["LoginClass2"]}`,
        Dlargs: `${vars["Dlargs2"]}`,
        MemberId: `${username}`,
        Password: `${password}`,
        __RequestVerificationToken: `${vars["__RequestVerificationToken2"]}`,
      },
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          origin: "https://login-qa.maintenanceconnection.com",
          "upgrade-insecure-requests": "1",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
        },
      }
    );
    

   
    vars["__RequestVerificationToken3"] = response
    .html()
    .find("input[name=__RequestVerificationToken]")
    .first()
    .attr("value");
    sleep(6.7);

    response = http.post(
      "https://login-qa.maintenanceconnection.com/",
      {
        LoginClass: `${vars["LoginClass2"]}`,
        Dlargs: `${vars["Dlargs2"]}`,
        MemberId: `${username}`,
        SelectedContainer: "07108C08-EB83-460A-8411-55D20902746C",
        moduleButton: "MRO",
        __RequestVerificationToken:
        `${vars["__RequestVerificationToken3"]}`,
      },
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          origin: "https://login-qa.maintenanceconnection.com",
          "upgrade-insecure-requests": "1",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"Windows"',
        },
      }
    );
   
    sleep(3.2);
  });

  group(
    "page_4 - https://www-qa.maintenanceconnection.com/mcv18/online/mc_login_form.asp",
    function () {
      response = http.get(
        "https://www-qa.maintenanceconnection.com/mcv18/online/mc_login_form.asp",
        {
          headers: {
            "upgrade-insecure-requests": "1",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Windows"',
          },
        }
      );
      
      
      sleep(7.9);
      
    

             
      response = http.get(
        "https://www-qa.maintenanceconnection.com/mcv18/online/mc_login.asp",
        {
          headers: {
            "upgrade-insecure-requests": "1",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Windows"',
          },
        }
      );
     
      sleep(3.2);
       
          
          const ck=JSON.stringify(response.cookies);
          const loop=ck.split(':"MROSCQa","value":"');
          let arr=(loop[1]);
          const mro=(arr.split('","domain":"maintenanceconnection.com","'));
          let durl=(mro[0]);
          // console.log(`https://www-qa.maintenanceconnection.com/mcv18/mapp_v80/default.asp?z=&s=${durl}&smallres=y`);


          response = http.get(
        "https://www-qa.maintenanceconnection.com/mcv18/online/languageio.asp?languageaction=LIST&_=1638880047161",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "x-requested-with": "XMLHttpRequest",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Windows"',
          },
        }
      );

          
        
        

          response = http.get(
            `https://www-qa.maintenanceconnection.com/mcv18/mapp_v80/default.asp?z=&s=${durl}&smallres=y`,
            {
              headers: {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
              "upgrade-insecure-requests": "1",
              "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
              "sec-ch-ua-mobile": "?0",
              "sec-ch-ua-platform": '"Windows"',
              },
            }
          );

          console.log(JSON.stringify(response.body));
          
          

          response = http.get(
            "https://login-qa.maintenanceconnection.com/CreateAuthCookieForMro",
            {
              headers: {
                "sec-ch-ua":
                  '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": '"Windows"',
              },
            }
          );      


    }
  );
  
}